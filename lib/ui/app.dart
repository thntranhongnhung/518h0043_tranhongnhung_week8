import 'package:flutter/material.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Login me',
      home: Scaffold(
        body: LoginScreen(),
      ),
    );
  }

}

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }

}
extension extString on String {

  bool get isValidPasswordUpperCase{
    final nameRegExp = new RegExp(r'^(?=.*?[A-Z]).{1,}$');

    return nameRegExp.hasMatch(this);
  }
  bool get isValidPasswordlowerCase{
    final nameRegExp = new RegExp(r'^(?=.*[a-z]).{1,}$');
    return nameRegExp.hasMatch(this);
  }
  bool get isValidPasswordSpecialCharacter{
    final nameRegExp = new RegExp(r'^(?=.*?[!@#\$&*~]).{1,}$');
    return nameRegExp.hasMatch(this);
  }




}
class LoginState extends State<StatefulWidget> {
  final formKey = GlobalKey<FormState>();
  late String email;
  late String password;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(20.0),
      child: Form(
        key: formKey,
        child: Column(
          children: [
            emailField(),
            Container(margin: EdgeInsets.only(top: 20.0),),
            passwordField(),
            Container(margin: EdgeInsets.only(top: 40.0),),
            loginButton()
          ],
        ),
      ),
    );
  }

  Widget emailField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          icon: Icon(Icons.person),
          labelText: 'Email address'
      ),
      validator: (value) {
        if (!value!.contains('@')) {
          return "Pls input valid email. Email not have @";
        }
        if(!value!.contains('.')){
          return "Pls input valid email.Email not have .";
        }


        return null;
      },

      onSaved: (value) {
        email = value as String;
      },

    );
  }

  Widget passwordField() {
    return TextFormField(
      obscureText: true,
      decoration: InputDecoration(
          icon: Icon(Icons.password),
          labelText: 'Password'
      ),
      validator: (value) {
        if (!value!.isValidPasswordUpperCase) {
          return "Có ít nhất 1 kí tự Hoa" ;
        }
        if (!value.isValidPasswordlowerCase) {
          return "Có ít nhất 1 kí tự thường.";
        }
        if (!value.isValidPasswordSpecialCharacter) {
          return "Có ít nhất 1 kí tự đặc biệt.";
        }
        if (value!.length < 8) {
          return "Password has at least 8 characters.";
        }

        return null;
      },
      onSaved: (value) {
        password = value as String;
      },
    );
  }

  Widget loginButton() {
    return ElevatedButton(
        onPressed: () {
          if (formKey.currentState!.validate()) {
            formKey.currentState!.save();

            print('email=$email');
            print('Demo only: password=$password');
          }
        },
        child: Text('Login')
    );
  }
}
